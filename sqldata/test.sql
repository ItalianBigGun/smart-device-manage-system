-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: test
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE IF NOT EXISTS `test`;

USE `test`;

--
-- Table structure for table `test_account`
--

DROP TABLE IF EXISTS `test_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_account` (
  `id` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_account`
--

LOCK TABLES `test_account` WRITE;
/*!40000 ALTER TABLE `test_account` DISABLE KEYS */;
INSERT INTO `test_account` VALUES ('202001','abc123',0),('202002','abc123',0);
/*!40000 ALTER TABLE `test_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `test_account_view`
--

DROP TABLE IF EXISTS `test_account_view`;
/*!50001 DROP VIEW IF EXISTS `test_account_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `test_account_view` AS SELECT 
 1 AS `id`,
 1 AS `password`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `test_admin`
--

DROP TABLE IF EXISTS `test_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_admin` (
  `id` varchar(64) NOT NULL,
  `password` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_admin`
--

LOCK TABLES `test_admin` WRITE;
/*!40000 ALTER TABLE `test_admin` DISABLE KEYS */;
INSERT INTO `test_admin` VALUES ('li','li'),('lovestory','hellolili');
/*!40000 ALTER TABLE `test_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_device`
--

DROP TABLE IF EXISTS `test_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_device` (
  `t_id` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `ISDN` varchar(64) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`t_id`,`name`,`ISDN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_device`
--

LOCK TABLES `test_device` WRITE;
/*!40000 ALTER TABLE `test_device` DISABLE KEYS */;
INSERT INTO `test_device` VALUES ('A1','LED1','LED-A001',1),('A1','LED2','LED-A001',0),('A1','LED3','LED-A001',0);
/*!40000 ALTER TABLE `test_device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_isdn`
--

DROP TABLE IF EXISTS `test_isdn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_isdn` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`no`,`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_isdn`
--

LOCK TABLES `test_isdn` WRITE;
/*!40000 ALTER TABLE `test_isdn` DISABLE KEYS */;
INSERT INTO `test_isdn` VALUES (1,'LED-A001'),(2,'LED-A002');
/*!40000 ALTER TABLE `test_isdn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_log`
--

DROP TABLE IF EXISTS `test_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_log` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `opt` varchar(64) DEFAULT NULL,
  `message` varchar(1024) DEFAULT NULL,
  `date` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_log`
--

LOCK TABLES `test_log` WRITE;
/*!40000 ALTER TABLE `test_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `test_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_table`
--

DROP TABLE IF EXISTS `test_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_table` (
  `id` int(4) DEFAULT NULL,
  `name` varchar(20) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_table`
--

LOCK TABLES `test_table` WRITE;
/*!40000 ALTER TABLE `test_table` DISABLE KEYS */;
INSERT INTO `test_table` VALUES (11,NULL),(1,'aaa'),(2,'bbb'),(3,'ccc'),(22,NULL),(33,NULL),(1,'aaa'),(2,'bbb'),(3,'ccc'),(11,NULL),(22,NULL),(33,NULL),(1,'aaa'),(11,NULL),(22,NULL),(33,NULL),(2,'bbb'),(3,'ccc'),(11,NULL),(1,'aaa'),(2,'bbb'),(22,NULL),(3,'ccc'),(33,NULL),(1,'aaa'),(2,'bbb'),(3,'ccc'),(11,NULL),(22,NULL),(33,NULL),(1,'aaa'),(2,'bbb'),(3,'ccc'),(11,NULL),(22,NULL),(33,NULL);
/*!40000 ALTER TABLE `test_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_terminal`
--

DROP TABLE IF EXISTS `test_terminal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_terminal` (
  `id` varchar(64) NOT NULL,
  `a_id` varchar(64) NOT NULL,
  `status` bigint(20) DEFAULT NULL,
  `controlstatus` int(11) NOT NULL,
  PRIMARY KEY (`a_id`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_terminal`
--

LOCK TABLES `test_terminal` WRITE;
/*!40000 ALTER TABLE `test_terminal` DISABLE KEYS */;
INSERT INTO `test_terminal` VALUES ('A1','202001',139801319775488,0);
/*!40000 ALTER TABLE `test_terminal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `test_account_view`
--

/*!50001 DROP VIEW IF EXISTS `test_account_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = gbk */;
/*!50001 SET character_set_results     = gbk */;
/*!50001 SET collation_connection      = gbk_chinese_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `test_account_view` AS select `test_account`.`id` AS `id`,`test_account`.`password` AS `password` from `test_account` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-31 17:41:20
