### 一 概述
- 1. 这是设备端，运行于支持Qt环境的专用设备上。
- 2. 默认使用了作者的驱动程序接口。如需移植，请改用其他的驱动接口。
- 3. 使用对应的平台的交叉编译工具。